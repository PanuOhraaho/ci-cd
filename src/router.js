const express = require('express');
const router = express();
const path = require('path');

// router endpoint
router.get("/", (req, res) => res.send("welcome!"));

// router endpoint to add two numbers together. Query parameters are "a" and "b".
router.get("/add", (req, res) => {
    try {
        const sum = req.query.a + req.query.b;
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

// Endpoint 3 POST http://localhost:3000/webhook-update
/** Proper JSON REST API Middlewares */
router.use(express.urlencoded({ extended: true }));
router.use(express.json());

router.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        console.error(err);
        return res.status(400).send({ status: 404, message: err.message }); // Bad request
    }
    next();
});

let updateState = 0;
const util = require('util');
router.post("/webhook-update", async (req, res, next) => {
    const default_branch = 'main'; // or master, check for yourself
    try {
        if (typeof req.headers['x-gitlab-token'] !== 'string') throw new Error('gitlab webhook token doesn`t meet the requirements');
        if (req.headers['x-gitlab-token'] !== process.env.WEBHOOK_TOKEN) throw new Error('gitlab webhook token doesn`t match environment');
        if (req.body.ref !== 'refs/heads/main') throw new Error(`only interested in the branch ${default_branch}`);
        if (updateState == 0) {
            updateState = 1;
            res.json({msg: 'OK'});
            const exec = util.promisify(require('child_process').exec);
            try {
                const update_script = path.join(__dirname, '..', process.env.UPDATE_SCRIPT);
                const {stdout, stderr} = await exec(update_script);
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
            } catch (e) {
                console.error(e);
            }
            process.exit(-1); // error to restart
        } else {
            res.status(500).json({msg: 'update currently on'});
        }
    } catch (e) {
        console.error(e);
        res.statusCode(500); // internal server error
    }
});

module.exports = router;
